export default [
  {
    name: 'Workflow',
    link: '/',
    icon: 'fa-home',
    child: [
      {
        name: 'Produção',
        link: '/user/new',
        icon: 'fa-circle-o',
      },
      {
        name: 'Recurso',
        link: '/user/list',
        icon: 'fa-circle-o'
      }
    ]
  },
  {
    name: 'Users',
    link: '/users',
    icon: 'fa-user',
    child: [
    {
      name: 'New',
      link: '/user/new',
      icon: 'fa-circle-o',
    },
    {
      name: 'Lists',
      link: '/user/list',
      icon: 'fa-circle-o'
    }
    ]
  },
  {
    name: 'Settings',
    link: '/settings',
    icon: 'fa-cogs',
    child: [
      {
        name: 'New',
        link: '/user/new',
        icon: 'fa-circle-o',
      },
      {
        name: 'Lists',
        link: '/user/list',
        icon: 'fa-circle-o'
      }
    ]
  }
]
