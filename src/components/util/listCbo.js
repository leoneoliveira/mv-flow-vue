export default [{ CD_CBO: '201115', DS_CBO: 'Geneticista' },
  { CD_CBO: '203013', DS_CBO: 'Pesquisador em Biologia de Micro-organismos e Parasitas' },
  { CD_CBO: '213150', DS_CBO: 'Físico Médico' },
  { CD_CBO: '221105', DS_CBO: 'Biólogo' },
  { CD_CBO: '223204', DS_CBO: 'Cirurgião Dentista - Auditor' },
  { CD_CBO: '223208', DS_CBO: 'Cirurgião Dentista - Clínico Geral' },
  { CD_CBO: '223212', DS_CBO: 'Cirurgião Dentista - Endodontista' },
  { CD_CBO: '223216', DS_CBO: 'Cirurgião Dentista - Epidemiologista' },
  { CD_CBO: '223220', DS_CBO: 'Cirurgião Dentista - Estomatologista' },
  { CD_CBO: '223224', DS_CBO: 'Cirurgião Dentista - Implantodontista' },
  { CD_CBO: '223228', DS_CBO: 'Cirurgião Dentista - Odontogeriatra' },
  { CD_CBO: '223232', DS_CBO: 'Cirurgião Dentista - Odontologista Legal' },
  { CD_CBO: '223236', DS_CBO: 'Cirurgião Dentista - Odontopediatra' },
  { CD_CBO: '223240', DS_CBO: 'Cirurgião Dentista - Ortopedista E Ortodontista' },
  { CD_CBO: '223244', DS_CBO: 'Cirurgião Dentista - Patologista Bucal' },
  { CD_CBO: '223248', DS_CBO: 'Cirurgião Dentista - Periodontista' },
  { CD_CBO: '223252', DS_CBO: 'Cirurgião Dentista - Protesiólogo Bucomaxilofacial' },
  { CD_CBO: '223256', DS_CBO: 'Cirurgião Dentista - Protesista' },
  { CD_CBO: '223260', DS_CBO: 'Cirurgião Dentista - Radiologista' },
  { CD_CBO: '223264', DS_CBO: 'Cirurgião Dentista - Reabilitador Oral' },
  { CD_CBO: '223268', DS_CBO: 'Cirurgião Dentista - Traumatologista Bucomaxilofacial' },
  { CD_CBO: '223272', DS_CBO: 'Cirurgião Dentista de Saúde Coletiva' },
  { CD_CBO: '223276', DS_CBO: 'Cirurgião Dentista – Odontologia do Trabalho' },
  { CD_CBO: '223280', DS_CBO: 'Cirurgião Dentista - Dentística' },
  { CD_CBO: '223284', DS_CBO: 'Cirurgião Dentista - Disfunção Temporomandibular e Dor Orofacial' },
  { CD_CBO: '223288', DS_CBO: 'Cirurgião Dentista - Odontologia para Pacientes com Necessidades Especiais' },
  { CD_CBO: '223293', DS_CBO: 'Cirurgião-Dentista da Estratégia de Saúde da Família' },
  { CD_CBO: '223505', DS_CBO: 'Enfermeiro' },
  { CD_CBO: '223605', DS_CBO: 'Fisioterapeuta Geral' },
  { CD_CBO: '223710', DS_CBO: 'Nutricionista' },
  { CD_CBO: '223810', DS_CBO: 'Fonoaudiólogo' },
  { CD_CBO: '223905', DS_CBO: 'Terapeuta Ocupacional' },
  { CD_CBO: '223910', DS_CBO: 'Ortoptista' },
  { CD_CBO: '225103', DS_CBO: 'Médico Infectologista' },
  { CD_CBO: '225105', DS_CBO: 'Médico Acupunturista' },
  { CD_CBO: '225106', DS_CBO: 'Médico Legista' },
  { CD_CBO: '225109', DS_CBO: 'Médico Nefrologista' },
  { CD_CBO: '225110', DS_CBO: 'Médico Alergista e Imunologista' },
  { CD_CBO: '225112', DS_CBO: 'Médico Neurologista' },
  { CD_CBO: '225115', DS_CBO: 'Médico Angiologista' },
  { CD_CBO: '225118', DS_CBO: 'Médico Nutrologista' },
  { CD_CBO: '225120', DS_CBO: 'Médico Cardiologista' },
  { CD_CBO: '225121', DS_CBO: 'Médico Oncologista Clínico' },
  { CD_CBO: '225122', DS_CBO: 'Médico Cancerologista Pediátrico' },
  { CD_CBO: '225124', DS_CBO: 'Médico Pediatra' },
  { CD_CBO: '225125', DS_CBO: 'Médico Clínico' },
  { CD_CBO: '225127', DS_CBO: 'Médico Pneumologista' },
  { CD_CBO: '225130', DS_CBO: 'Médico de Família e Comunidade' },
  { CD_CBO: '225133', DS_CBO: 'Médico Psiquiatra' },
  { CD_CBO: '225135', DS_CBO: 'Médico Dermatologista' },
  { CD_CBO: '225136', DS_CBO: 'Médico Reumatologista' },
  { CD_CBO: '225139', DS_CBO: 'Médico Sanitarista' },
  { CD_CBO: '225140', DS_CBO: 'Médico do Trabalho' },
  { CD_CBO: '225142', DS_CBO: 'Médico da Estratégia de Saúde da Família' },
  { CD_CBO: '225145', DS_CBO: 'Médico em Medicina de Tráfego' },
  { CD_CBO: '225148', DS_CBO: 'Médico Anatomopatologista' },
  { CD_CBO: '225150', DS_CBO: 'Médico em Medicina Intensiva' },
  { CD_CBO: '225151', DS_CBO: 'Médico Anestesiologista' },
  { CD_CBO: '225155', DS_CBO: 'Médico Endocrinologista e Metabologista' },
  { CD_CBO: '225160', DS_CBO: 'Médico Fisiatra' },
  { CD_CBO: '225165', DS_CBO: 'Médico Gastroenterologista' },
  { CD_CBO: '225170', DS_CBO: 'Médico Generalista' },
  { CD_CBO: '225175', DS_CBO: 'Médico Geneticista' },
  { CD_CBO: '225180', DS_CBO: 'Médico Geriatra' },
  { CD_CBO: '225185', DS_CBO: 'Médico Hematologista' },
  { CD_CBO: '225195', DS_CBO: 'Médico Homeopata' },
  { CD_CBO: '225203', DS_CBO: 'Médico em Cirurgia Vascular' },
  { CD_CBO: '225210', DS_CBO: 'Médico Cirurgião Cardiovascular' },
  { CD_CBO: '225215', DS_CBO: 'Médico Cirurgião de Cabeça e Pescoço' },
  { CD_CBO: '225220', DS_CBO: 'Médico Cirurgião do Aparelho Digestivo' },
  { CD_CBO: '225225', DS_CBO: 'Médico Cirurgião Geral' },
  { CD_CBO: '225230', DS_CBO: 'Médico Cirurgião Pediátrico' },
  { CD_CBO: '225235', DS_CBO: 'Médico Cirurgião Plástico' },
  { CD_CBO: '225240', DS_CBO: 'Médico Cirurgião Torácico' },
  { CD_CBO: '225250', DS_CBO: 'Médico Ginecologista e Obstetra' },
  { CD_CBO: '225255', DS_CBO: 'Médico Mastologista' },
  { CD_CBO: '225260', DS_CBO: 'Médico Neurocirurgião' },
  { CD_CBO: '225265', DS_CBO: 'Médico Oftalmologista' },
  { CD_CBO: '225270', DS_CBO: 'Médico Ortopedista e Traumatologista' },
  { CD_CBO: '225275', DS_CBO: 'Médico Otorrinolaringologista' },
  { CD_CBO: '225280', DS_CBO: 'Médico Proctologista' },
  { CD_CBO: '225285', DS_CBO: 'Médico Urologista' },
  { CD_CBO: '225290', DS_CBO: 'Médico Cancerologista Cirúrgico' },
  { CD_CBO: '225295', DS_CBO: 'Médico Cirurgião da Mão' },
  { CD_CBO: '225305', DS_CBO: 'Médico Citopatologista' },
  { CD_CBO: '225310', DS_CBO: 'Médico em Endoscopia' },
  { CD_CBO: '225315', DS_CBO: 'Médico em Medicina Nuclear' },
  { CD_CBO: '225320', DS_CBO: 'Médico em Radiologia e Diagnóstico por Imagem ' },
  { CD_CBO: '225325', DS_CBO: 'Médico Patologista' },
  { CD_CBO: '225330', DS_CBO: 'Médico Radioterapeuta' },
  { CD_CBO: '225335', DS_CBO: 'Médico Patologista Clínico / Medicina Laboratorial' },
  { CD_CBO: '225340', DS_CBO: 'Médico Hemoterapeuta' },
  { CD_CBO: '225345', DS_CBO: 'Médico Hiperbarista' },
  { CD_CBO: '225350', DS_CBO: 'Médico Neurofisiologista' },
  { CD_CBO: '239425', DS_CBO: 'Psicopedagogo' },
  { CD_CBO: '251510', DS_CBO: 'Psicólogo Clínico' },
  { CD_CBO: '251545', DS_CBO: 'Neuropsicólogo' },
  { CD_CBO: '251550', DS_CBO: 'Psicanalista' },
  { CD_CBO: '251605', DS_CBO: 'Assistente Social' },
  { CD_CBO: '322205', DS_CBO: 'Técnico de Enfermagem' },
  { CD_CBO: '322220', DS_CBO: 'Técnico de Enfermagem Psiquiátrica' },
  { CD_CBO: '322225', DS_CBO: 'Instrumentador Cirúrgico' },
  { CD_CBO: '322230', DS_CBO: 'Auxiliar de Enfermagem' },
  { CD_CBO: '516210', DS_CBO: 'Cuidador de Idosos' },
  { CD_CBO: '999999', DS_CBO: 'CBO Desconhecido' }];
